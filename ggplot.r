
#GRAPHIQUES UNIVARIES ET GRAPHIQUES BIVARIES----

# Importation de la librairie----

library(tidyverse)
library(questionr)
library(gtsummary)
library(labelled)

# Importation du jeu de données : hdv2003 et trial

# Données hdv2003: d'étude de l'INSEE en 2003
data(hdv2003)
Donnee_Hdv = hdv2003

# Données trial : étude clinique(cancer)

data(trial)
Trial_donnees = trial 
Trial_donnees


# Vu synthétique sur le jeu de données 

#look_for(Donnee_Hdv) # ou encore Donnees %>% look_for(): ça donne meme résultat

summary(Trial_donnees)


# GRAPHIQUES----

# Le cas de variable univariée: ici une variable en continu----

ggplot(hdv2003)+
  aes(x = age)+
  geom_histogram()

# Précision du nombre de barres avec bins
ggplot(hdv2003)+
  aes(x = age)+
  geom_histogram(bins = 10, fill= "orange", color = "black")


# un peu plus de couleur et de personnalisation.

ggplot(hdv2003)+
  aes(x = age)+
  geom_histogram(breaks = c(18, seq(20, 95, by = 5), 97), fill= "orange", color = "black")

# A noter que le break a une limite de ne pas respecter la forme de 
#l'échantillonnage, d'où l'alternative ci-dessous. il faut plustot répresenter 
#le nombre d'observation par la largeur des barres. 

ggplot(hdv2003)+
  aes(x = age, y = after_stat(count)/ after_stat(width))+
  geom_histogram(breaks = c(18,20,30,50,80,97), fill= "orange", color = "black")


Donnee_Hdv %>% filter(age >= 80 ) %>% nrow() # formule pour filter


# Une variable continue et une variable catégorielle----


ggplot(Donnee_Hdv)+
  aes(x = age, fill= sexe)+
  geom_histogram()+
  facet_grid(rows = vars(sexe))

# Courbe de densité selon le sexe

ggplot(Donnee_Hdv)+
  aes(x = age, color = sexe)+
  geom_density()

ggplot(Donnee_Hdv)+
  aes(x = age, y = after_stat(density))+
  geom_histogram(fill= "maroon", alpha= .5, color = "orange")+
  geom_density(adjust = 2)+ # Adjust pour lisser
  facet_grid(cols = vars(sexe))

# La boite à moustache pour la comparaison d'une variable quantitative et catégorielle

  
ggplot(Donnee_Hdv)+
  aes(x = sexe, y = age)+
  geom_boxplot()

# Une comparaison en fonction de la pratique du sport au niveau des deux genres

ggplot(Donnee_Hdv)+
  aes(x = sexe, y = age, fill= sport)+
  geom_boxplot()

# La boite à moustache à souvent l'inconvénient d'etre trop synthétique et ne 
# répresente pas vraiment la distribution de la population. D'autres préfèrent 
#ainsi le violin graph


ggplot(Donnee_Hdv)+
  aes(x = sexe, y = age, fill= sport)+
  geom_violin()

#NB:on peut utiliser annotate pour les annotations.L'aide à propos: ?annotate

ggplot(Donnee_Hdv)+
  aes(x = sexe,y = age, fill= sport)+
  geom_boxplot()+
  annotate("text", x = 1.5, y = 85, label = "MON ETIQUETTE")



# Les variables catégorielle: les diagrammes en barres----


ggplot(Trial_donnees)+
  aes(x = stage)+
  geom_bar()
  
# Affichage en fonction du grade 


ggplot(Trial_donnees)+
  aes(x = stage, fill = grade)+
  geom_bar(position = "dodge")# affichage cote à cote 


ggplot(Trial_donnees)+
  aes(x = stage, fill = grade)+
  geom_bar(position = "fill")


# Avec des étiquètes lle nombre d'observations

ggplot(Trial_donnees)+
  aes(x = stage, fill = grade, label = after_stat(count))+
  geom_bar()+
  geom_text(stat = "count", position = position_stack(.5))


# Meme résultat seulment j'indique vouloir des esthétiques valable que pour cette gométrie
ggplot(Trial_donnees)+
  aes(x = stage, fill = grade)+
  geom_bar()+
  geom_text(
    mapping = aes(label = after_stat(count)),
    stat = "count", 
    position = position_stack(.5))



ggplot(Trial_donnees)+
  aes(x = stage, fill = grade, label = after_stat(count))+
  geom_bar(position = "dodge")+# affichage cote à cote 
  geom_text(stat = "count", position = position_dodge(.9))








